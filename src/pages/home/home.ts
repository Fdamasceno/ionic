
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators/map';
import { ToastController } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  img: any;
  users: any;
  num: any;

  constructor(public http: Http, public navCtrl: NavController, private toastCtrl: ToastController) {
    this.getUsers(1);
    
  }
   
  getUsers(limit){   
      this.http.get('https://reqres.in/api/users?per_page=5&page='+limit)
      .subscribe(data => {
        this.users = data.json();
        this.users = this.users.data;
        this.num   = limit;
        return this.users;
      });
  }

  
  
  presentToast(users) {
    let toast = this.toastCtrl.create({
      message: 'Nome: '+users.first_name+' Apelido: '+users.last_name,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }  
}


